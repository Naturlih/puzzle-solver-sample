import model.*;
import org.junit.Test;
import service.PuzzleSideSuitabilityCheckerImpl;
import service.PuzzleSolver;

import java.util.*;

import static org.junit.Assert.*;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class SolverTest {
    @Test
    public void checkCorrectSolvingOfPuzzle() {
        List<PuzzlePiece<SelfCheckingPuzzlePieceSide>> pieces = getCorrectPuzzlePiecesSample();

        shufflePieces(pieces);

        PuzzleSolver<SelfCheckingPuzzlePieceSide> solver = new PuzzleSolver<>();
        PuzzleSolution puzzleSolution = solver.solvePuzzle(pieces, new PuzzleSideSuitabilityCheckerImpl());
        assertTrue("Puzzle should be solved, if pieces are valid", puzzleSolution.isSolved());
    }

    @Test
    public void checkCorrectNotSolvingOfPuzzle() {
        List<PuzzlePiece<SelfCheckingPuzzlePieceSide>> pieces = getCorrectPuzzlePiecesSample();
        PuzzlePiece<SelfCheckingPuzzlePieceSide> corruptedPiece = pieces.get(1);
        //duplicate of first corner piece
        PuzzlePiece<SelfCheckingPuzzlePieceSide> leftTopCornerPiece = pieces.get(0);
        corruptedPiece = new PuzzlePiece<>(corruptedPiece.getId(), leftTopCornerPiece.getSide(PuzzleSidePlace.TOP),
                leftTopCornerPiece.getSide(PuzzleSidePlace.RIGHT), leftTopCornerPiece.getSide(PuzzleSidePlace.TOP),
                leftTopCornerPiece.getSide(PuzzleSidePlace.LEFT));
        pieces.set(0, corruptedPiece);

        shufflePieces(pieces);

        PuzzleSolver<SelfCheckingPuzzlePieceSide> solver = new PuzzleSolver<>();
        PuzzleSolution puzzleSolution = solver.solvePuzzle(pieces, new PuzzleSideSuitabilityCheckerImpl());
        assertFalse("Puzzle should not be solved, if pieces are invalid", puzzleSolution.isSolved());
    }

    private List<PuzzlePiece<SelfCheckingPuzzlePieceSide>> getCorrectPuzzlePiecesSample() {
        int i = 0;
        List<PuzzlePiece<SelfCheckingPuzzlePieceSide>> pieces = new ArrayList<>();
        pieces.add(new PuzzlePiece<>(i++,
                new PuzzlePieceOuterSide(),
                new PuzzlePieceInnerSide(1, true),
                new PuzzlePieceInnerSide(4, true),
                new PuzzlePieceOuterSide()));
        pieces.add(new PuzzlePiece<>(i++,
                new PuzzlePieceOuterSide(),
                new PuzzlePieceInnerSide(2, true),
                new PuzzlePieceInnerSide(3, true),
                new PuzzlePieceInnerSide(1, false)));
        pieces.add(new PuzzlePiece<>(i++,
                new PuzzlePieceOuterSide(),
                new PuzzlePieceOuterSide(),
                new PuzzlePieceInnerSide(7, true),
                new PuzzlePieceInnerSide(2, false)));
        pieces.add(new PuzzlePiece<>(i++,
                new PuzzlePieceInnerSide(4, false),
                new PuzzlePieceInnerSide(7, true),
                new PuzzlePieceInnerSide(8, true),
                new PuzzlePieceOuterSide()));
        pieces.add(new PuzzlePiece<>(i++,
                new PuzzlePieceInnerSide(3, false),
                new PuzzlePieceInnerSide(4, true),
                new PuzzlePieceInnerSide(9, true),
                new PuzzlePieceInnerSide(7, false)));
        pieces.add(new PuzzlePiece<>(i++,
                new PuzzlePieceInnerSide(7, false),
                new PuzzlePieceOuterSide(),
                new PuzzlePieceInnerSide(10, true),
                new PuzzlePieceInnerSide(4, false)));
        pieces.add(new PuzzlePiece<>(i++,
                new PuzzlePieceInnerSide(8, false),
                new PuzzlePieceInnerSide(11, true),
                new PuzzlePieceOuterSide(),
                new PuzzlePieceOuterSide()));
        pieces.add(new PuzzlePiece<>(i++,
                new PuzzlePieceInnerSide(9, false),
                new PuzzlePieceInnerSide(3, true),
                new PuzzlePieceOuterSide(),
                new PuzzlePieceInnerSide(11, false)));
        pieces.add(new PuzzlePiece<>(i,
                new PuzzlePieceInnerSide(10, false),
                new PuzzlePieceOuterSide(),
                new PuzzlePieceOuterSide(),
                new PuzzlePieceInnerSide(3, false)));

        return pieces;
    }

    private <T extends PuzzlePieceSide> void shufflePieces(List<PuzzlePiece<T>> pieces) {
        Random r = new Random(123);

        Collections.shuffle(pieces, r);

        for (int j = 0; j < pieces.size(); j++) {
            PuzzlePiece<T> piece = pieces.get(j);
            for (int k = 0; k < r.nextInt(4); k++) {
                piece = piece.getRotated();
            }
            pieces.set(j, piece);
        }
    }
}
