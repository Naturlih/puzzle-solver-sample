package service;

import model.PuzzlePiece;
import model.PuzzlePieceSide;
import model.PuzzleSidePlace;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public interface PuzzleSideSuitabilityChecker<T extends PuzzlePieceSide> {
    /**
     * Checks are two sides of puzzle suitable for each other.
     *
     * @param firstPiece      first piece to check
     * @param firstSidePlace  side place of first piece
     * @param secondPiece     second piece to check
     * @param secondSidePlace side place of second piece
     * @return true, if sides are complement to each other
     */
    boolean areSuitable(PuzzlePiece<T> firstPiece, PuzzleSidePlace firstSidePlace, PuzzlePiece<T> secondPiece,
                        PuzzleSidePlace secondSidePlace);
}
