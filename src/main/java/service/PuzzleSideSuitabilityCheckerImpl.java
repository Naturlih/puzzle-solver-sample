package service;

import model.PuzzlePiece;
import model.PuzzleSidePlace;
import model.SelfCheckingPuzzlePieceSide;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class PuzzleSideSuitabilityCheckerImpl implements PuzzleSideSuitabilityChecker<SelfCheckingPuzzlePieceSide> {
    /**
     * Checks are two sides of puzzle suitable for each other.
     *
     * @param firstPiece      first piece to check
     * @param firstSidePlace  side place of first piece
     * @param secondPiece     second piece to check
     * @param secondSidePlace side place of second piece
     * @return true, if sides are complement to each other
     */
    public boolean areSuitable(PuzzlePiece<SelfCheckingPuzzlePieceSide> firstPiece, PuzzleSidePlace firstSidePlace,
                               PuzzlePiece<SelfCheckingPuzzlePieceSide> secondPiece, PuzzleSidePlace secondSidePlace) {
        return firstPiece.getSide(firstSidePlace).isSuitableFor(secondPiece.getSide(secondSidePlace));
    }
}
