package service;

import model.*;

import java.util.*;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class PuzzleSolver<T extends PuzzlePieceSide> {
    /**
     * Tries to solve puzzle of specified pieces.
     *
     * @param pieces pieces to combine into puzzle
     * @return solution of puzzle
     * @throws IllegalArgumentException if width * height is not equal to pieces.size()
     */
    public PuzzleSolution solvePuzzle(Collection<PuzzlePiece<T>> pieces, PuzzleSideSuitabilityChecker<T> checker) {
        Set<PuzzleDimension> dimensions = getAllAvailableDimensions(pieces.size());
        for (PuzzleDimension dimension : dimensions) {
            Puzzle puzzle = new Puzzle(dimension, checker);
            boolean completed = trySolvePuzzle(puzzle, new ArrayList<>(pieces), 0, 0);
            if (completed) {
                return PuzzleSolution.getSolvedSolution(puzzle.getPuzzleMap());
            }
        }

        return PuzzleSolution.getNotSolvedSolution();
    }

    private Set<PuzzleDimension> getAllAvailableDimensions(int size) {
        Set<PuzzleDimension> dimensions = new HashSet<>();

        for (int width = 1; width <= size; width++) {
            if (size % width == 0) {
                dimensions.add(new PuzzleDimension(width, size / width));
            }
        }

        return dimensions;
    }


    /**
     * Recursively solves puzzle by brutforcing through all possibilities of combinations of puzzles.
     *
     * @param puzzle          puzzle to insert pieces in
     * @param piecesLeftToAdd pieces, which are not yet inserted in current step
     * @param x               x of puzzle node to insert piece
     * @param y               y of puzzle node to insert piece
     * @return true, if puzzle is fully filled and solved, false, if puzzle cannot be solved
     */
    private boolean trySolvePuzzle(Puzzle puzzle, List<PuzzlePiece> piecesLeftToAdd, int x, int y) {
        if (piecesLeftToAdd.size() == 0) {
            return true;
        }

        for (int i = 0; i < piecesLeftToAdd.size(); i++) {
            PuzzlePiece piece = piecesLeftToAdd.get(i);
            for (int j = 0; j < piece.getSidesCount(); j++) {
                if (puzzle.isPuzzlePieceSuitableForSpot(piece, x, y)) {
                    puzzle.setPuzzlePiece(piece, x, y);
                    List<PuzzlePiece> newPiecesLeftToAdd = new ArrayList<>(piecesLeftToAdd);
                    newPiecesLeftToAdd.remove(i);
                    boolean completed;
                    int newX, newY;
                    if (x >= puzzle.getWidth() - 1) {
                        newX = 0;
                        newY = y + 1;
                    } else {
                        newX = x + 1;
                        newY = y;
                    }

                    completed = trySolvePuzzle(puzzle, newPiecesLeftToAdd, newX, newY);

                    if (completed) {
                        return true;
                    }

                    puzzle.removePiece(x, y);
                }

                piece = piece.getRotated();
            }
        }

        return false;
    }
}
