package model;

/**
 * Represents outer side of piece, i.e. edge side.
 *
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class PuzzlePieceOuterSide implements SelfCheckingPuzzlePieceSide {
    /**
     * Returns false, because edge side is never suitable for any other side.
     *
     * @param puzzlePieceSide side to check suitability
     * @return always false
     */
    public boolean isSuitableFor(PuzzlePieceSide puzzlePieceSide) {
        return false;
    }

    @Override
    public boolean isOuter() {
        return true;
    }
}
