package model;

/**
 * Puzzle piece side, which can check completion to other side itself.
 *
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public interface SelfCheckingPuzzlePieceSide extends PuzzlePieceSide {
    /**
     * Checks if other side is suitable for this side
     *
     * @param side side to check suitability
     * @return true, if puzzle side is suitable for this side
     */
    boolean isSuitableFor(PuzzlePieceSide side);
}
