package model;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class PuzzleDimension {
    private final int width;
    private final int height;

    public PuzzleDimension(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return "PuzzleDimension{" +
                "width=" + width +
                ", height=" + height +
                '}';
    }
}
