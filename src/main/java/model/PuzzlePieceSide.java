package model;

/**
 * Represents puzzle piece side
 *
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public interface PuzzlePieceSide {
    /**
     * Returns true, if side must be on edge of puzzle
     *
     * @return true, if side must be on edge of puzzle
     */
    boolean isOuter();
}
