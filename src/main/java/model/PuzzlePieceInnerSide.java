package model;

/**
 * Represents inner side of piece, i.e. side with ledge or recess. For each side type there is 2 possible puzzle sides
 * , one with ledge and one with recess.
 *
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class PuzzlePieceInnerSide implements SelfCheckingPuzzlePieceSide {
    private final int id;
    private final boolean hasRecess;

    public PuzzlePieceInnerSide(int id, boolean hasRecess) {
        this.id = id;
        this.hasRecess = hasRecess;
    }

    @Override
    public boolean isOuter() {
        return false;
    }

    public int getId() {
        return id;
    }

    public boolean hasRecess() {
        return hasRecess;
    }

    /**
     * Checks is side suitable for this side. Other side is suitable iff it has same type and side's recession is opposite
     * to recession of this side.
     *
     * @param side side to check suitability to
     * @return true, if side is suitable to this side
     */
    public boolean isSuitableFor(PuzzlePieceSide side) {
        if (this.getClass() != side.getClass()) {
            return false;
        }
        PuzzlePieceInnerSide innerSide = ((PuzzlePieceInnerSide) side);
        return this.getId() == innerSide.getId() && this.hasRecess() != innerSide.hasRecess();
    }
}
