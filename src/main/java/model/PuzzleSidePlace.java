package model;

/**
 * Represents direction of side.
 *
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public enum PuzzleSidePlace {
    TOP, RIGHT, BOTTOM, LEFT
}
