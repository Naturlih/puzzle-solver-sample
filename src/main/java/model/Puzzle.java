package model;

import service.PuzzleSideSuitabilityChecker;

import static model.PuzzleSidePlace.*;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class Puzzle {
    // Coordinate map for puzzle is as follows:
    // 0     X    width
    // *----------->
    // |
    // |
    // |
    // | Y
    // |
    // |
    // |
    // V height
    private PuzzlePiece[][] puzzleMap;
    private final PuzzleDimension dimension;
    private final PuzzleSideSuitabilityChecker suitnessChecker;

    /**
     * Creates new empty puzzle.
     * @param dimension dimension of puzzle
     * @param suitnessChecker check of suitness for sides.
     * @throws IllegalArgumentException if dimension width or height is less than 1.
     */
    public Puzzle(PuzzleDimension dimension, PuzzleSideSuitabilityChecker suitnessChecker) {
        if (dimension.getWidth() < 1 || dimension.getHeight() < 1) {
            throw new IllegalArgumentException("Width and height cannot be less than 1, but actually dimension is " + dimension);
        }
        this.puzzleMap = new PuzzlePiece[dimension.getWidth()][dimension.getHeight()];
        this.suitnessChecker = suitnessChecker;
        this.dimension = dimension;
    }

    public int getWidth() {
        return dimension.getWidth();
    }

    public int getHeight() {
        return dimension.getHeight();
    }

    public void setPuzzlePiece(PuzzlePiece piece, int x, int y) {
        if (!isPuzzlePieceSuitableForSpot(piece, x, y)) {
            throw new IllegalArgumentException("Cannot set piece.");
        }

        puzzleMap[x][y] = piece;
    }

    public void removePiece(int x, int y) {
        if (puzzleMap[x][y] == null) {
            throw new IllegalArgumentException("Cannot clean already empty spot.");
        }

        puzzleMap[x][y] = null;
    }

    public PuzzlePiece[][] getPuzzleMap() {
        return puzzleMap;
    }

    public boolean isPuzzlePieceSuitableForSpot(PuzzlePiece piece, int x, int y) {
        if (x == 0) {
            if (!piece.isOuterSide(LEFT)) {
                return false;
            }
        } else {
            PuzzlePiece nearestPiece = puzzleMap[x - 1][y];
            if (nearestPiece != null && !suitnessChecker.areSuitable(nearestPiece, RIGHT, piece, LEFT)) {
                return false;
            }
        }

        if (x == getWidth() - 1) {
            if (!piece.isOuterSide(RIGHT)) {
                return false;
            }
        } else {
            PuzzlePiece nearestPiece = puzzleMap[x + 1][y];
            if (nearestPiece != null && !suitnessChecker.areSuitable(nearestPiece, LEFT, piece, RIGHT)) {
                return false;
            }
        }

        if (y == 0) {
            if (!piece.isOuterSide(TOP)) {
                return false;
            }
        } else {
            PuzzlePiece nearestPiece = puzzleMap[x][y - 1];
            if (nearestPiece != null && !suitnessChecker.areSuitable(nearestPiece, BOTTOM, piece, TOP)) {
                return false;
            }
        }

        if (y == getHeight() - 1) {
            if (!piece.isOuterSide(BOTTOM)) {
                return false;
            }
        } else {
            PuzzlePiece nearestPiece = puzzleMap[x][y + 1];
            if (nearestPiece != null && !suitnessChecker.areSuitable(nearestPiece, TOP, piece, BOTTOM)) {
                return false;
            }
        }

        return true;
    }
}
