package model;

import java.util.LinkedList;

/**
 * Represents piece of puzzle.
 *
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class PuzzlePiece<T extends PuzzlePieceSide> {
    private final int id;
    private final LinkedList<T> sides;

    /**
     * Creates puzzle piece.
     */
    public PuzzlePiece(int id, T topSide, T rightSide, T bottomSide, T leftSide) {
        this.sides = new LinkedList<>();
        this.sides.add(topSide);
        this.sides.add(rightSide);
        this.sides.add(bottomSide);
        this.sides.add(leftSide);
        this.id = id;
    }

    private PuzzlePiece(int id, LinkedList<T> sides) {
        this.id = id;
        this.sides = sides;
    }

    /**
     * Returns new rotated piece.
     *
     * @return rotated piece
     */
    public PuzzlePiece<T> getRotated() {
        LinkedList<T> rotatedSides = new LinkedList<>(sides);
        T side = rotatedSides.remove();
        rotatedSides.addLast(side);

        return new PuzzlePiece<>(id, sides);
    }

    public int getId() {
        return id;
    }

    /**
     * Returns count of sides in piece.
     *
     * @return count of sides in piece
     */
    public int getSidesCount() {
        return sides.size();
    }

    public T getSide(PuzzleSidePlace align) {
        switch (align) {
            case TOP:
                return sides.get(0);
            case RIGHT:
                return sides.get(1);
            case BOTTOM:
                return sides.get(2);
            case LEFT:
                return sides.get(3);
            default:
                throw new IllegalArgumentException("Unexpected align " + align);
        }
    }

    public boolean isOuterSide(PuzzleSidePlace align) {
        return getSide(align).isOuter();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PuzzlePiece that = (PuzzlePiece) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
