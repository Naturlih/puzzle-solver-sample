package model;

/**
 * Solution of puzzle. If no solution can be found, then puzzle map is null.
 *
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class PuzzleSolution {
    private final PuzzlePiece[][] puzzleMap;

    public static PuzzleSolution getSolvedSolution(PuzzlePiece[][] puzzleMap) {
        return new PuzzleSolution(puzzleMap);
    }

    public static PuzzleSolution getNotSolvedSolution() {
        return new PuzzleSolution(null);
    }

    private PuzzleSolution(PuzzlePiece[][] puzzleMap) {
        this.puzzleMap = puzzleMap;
    }

    public boolean isSolved() {
        return puzzleMap != null;
    }

    public PuzzlePiece[][] getPuzzleMap() {
        return puzzleMap;
    }
}
